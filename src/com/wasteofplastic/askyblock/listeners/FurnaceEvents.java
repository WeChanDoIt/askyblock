package com.wasteofplastic.askyblock.listeners;

import org.bukkit.block.Furnace;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.FurnaceBurnEvent;

public class FurnaceEvents implements Listener {

    @EventHandler
    public void onFuel(FurnaceBurnEvent event) {
        int num = 80;
        int per = (event.getBurnTime() / 100) * num;
        event.setBurnTime(event.getBurnTime() + per);
    }

}
