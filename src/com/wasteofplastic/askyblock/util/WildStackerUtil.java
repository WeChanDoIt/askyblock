package com.wasteofplastic.askyblock.util;

import com.bgsoftware.wildstacker.api.WildStackerAPI;
import org.bukkit.Location;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;

public class WildStackerUtil {

    public static int getSpawnerAmount(Location location){
        return WildStackerAPI.getSpawnersAmount((CreatureSpawner) location.getBlock().getState());
    }

    public static EntityType getSpawnerType(Location location) {
        return WildStackerAPI.getStackedSpawner((CreatureSpawner) location.getBlock().getState()).getSpawnedType();
    }

    public static int getBarrelAmount(Location location){
        return WildStackerAPI.getBarrelAmount(location.getBlock());
    }

    public static ItemStack getBarrelItem(Location location){
        return WildStackerAPI.getStackedBarrel(location.getBlock()).getBarrelItem(1);
    }

    public static boolean isStackedBarrel(Location location){
        return WildStackerAPI.getWildStacker().getSystemManager().isStackedBarrel(location.getBlock());
    }

}